//
//  ATMMO+CoreDataClass.swift
//  ATMMap
//
//  Created by Yury Khadatovich on 12.03.22.
//
//

import UIKit
import CoreData

@objc(ATMMO)
public class ATMMO: NSManagedObject {
    public init?(addressLine: String,
                 atmId: String,
                 availability: [DayMO],
                 cashIn: Bool,
                 currency: String,
                 latitude: String,
                 longtitude: String,
                 townName: String
    ) {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return nil }

        let container = appDelegate.persistentContainer
        let managedContext = container.viewContext
        guard let entity = NSEntityDescription.entity(forEntityName: String(describing: ATMMO.self), in: managedContext) else { return nil }
        super.init(entity: entity, insertInto: managedContext)
        self.addressLine = addressLine
        self.atmId = atmId
        self.availability = availability
        self.cashIn = cashIn
        self.currency = currency
        self.latitude = latitude
        self.longtitude = longtitude
        self.townName = townName
    }
    public override init(entity: NSEntityDescription, insertInto context: NSManagedObjectContext?) {
        super.init(entity: entity, insertInto: context)
    }
}

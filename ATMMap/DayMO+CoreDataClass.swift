//
//  DayMO+CoreDataClass.swift
//  ATMMap
//
//  Created by Yury Khadatovich on 12.03.22.
//
//

import UIKit
import CoreData

@objc(DayMO)
public class DayMO: NSManagedObject, NSSecureCoding {
    public static var supportsSecureCoding: Bool {
        return true
    }

    public func encode(with coder: NSCoder) {
    }

    public required init?(coder: NSCoder) {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return nil }
        let container = appDelegate.persistentContainer
        let managedContext = container.viewContext
        guard let entity = NSEntityDescription.entity(forEntityName: "DayMO", in: managedContext) else { return nil }
        super.init(entity: entity, insertInto: managedContext)
    }

    public init?(closingTime: String, dayCode: String, openingTime: String, atm: ATMMO) {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return nil }
        let container = appDelegate.persistentContainer
        let managedContext = container.viewContext
        guard let entity = NSEntityDescription.entity(forEntityName: "DayMO", in: managedContext) else { return nil }
        super.init(entity: entity, insertInto: managedContext)
        self.closingTime = closingTime
        self.dayCode = dayCode
        self.openingTime = openingTime
        self.atm = atm
    }

    public override func awakeFromInsert() {
        super.awakeFromInsert()
        
    }
}

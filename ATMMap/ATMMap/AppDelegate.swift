//
//  AppDelegate.swift
//  ATMMap
//
//  Created by Yury Khadatovich on 23.02.22.
//

import CoreData
import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    private let locationManager = LocationManager.shared

    func applicationWillEnterForeground(_ application: UIApplication) {
        locationManager.stopUpdatingLocation()
    }

    // MARK: - Core Data stack

    lazy var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: Constants.Strings.dataBaseFileName)
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }

}

private extension AppDelegate {
    enum Constants {
        enum Strings {
            static let dataBaseFileName = "ATMMO"
        }
    }
}

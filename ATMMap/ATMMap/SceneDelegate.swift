//
//  SceneDelegate.swift
//  ATMMap
//
//  Created by Yury Khadatovich on 23.02.22.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?

    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        guard let windowScene = (scene as? UIWindowScene) else { return }
        let mainViewController = HomeViewController()
        let navigationController = UINavigationController(rootViewController: mainViewController)
        window = UIWindow(frame: windowScene.screen.bounds)
        window?.windowScene = windowScene
        window?.rootViewController = navigationController
        window?.overrideUserInterfaceStyle = .light
        window?.makeKeyAndVisible()
//        DataBaseManager.shared.fetchDataFromDB()
    }

}

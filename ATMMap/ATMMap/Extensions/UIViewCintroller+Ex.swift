//
//  UIViewCintroller+Ex.swift
//  ATMMap
//
//  Created by Yury Khadatovich on 12.03.22.
//

import UIKit

extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)

    }

    @objc
    func dismissKeyboard() {
        view.endEditing(true)
    }
}

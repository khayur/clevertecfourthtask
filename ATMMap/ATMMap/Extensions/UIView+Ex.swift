//
//  UIView+Ex.swift
//  ATMMap
//
//  Created by Yury Khadatovich on 21.03.22.
//

import Foundation
import UIKit

extension UIView {
    static let defaultPadding: CGFloat = 5
    static let cornerRadius: CGFloat = 7
}

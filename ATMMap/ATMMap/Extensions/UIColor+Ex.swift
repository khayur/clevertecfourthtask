//
//  UIColor+Ex.swift
//  ATMMap
//
//  Created by Yury Khadatovich on 21.03.22.
//

import UIKit

extension UIColor {
    static let appMainColor = UIColor(red: 58 / 255, green: 124 / 255, blue: 80 / 255, alpha: 1)
}

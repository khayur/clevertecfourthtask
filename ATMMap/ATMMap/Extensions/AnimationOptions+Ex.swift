//
//  AnimationOptions+Ex.swift
//  ATMMap
//
//  Created by Yury Khadatovich on 21.03.22.
//

import UIKit

extension UIView.AnimationOptions {
    static let animationDuration = 0.25
}

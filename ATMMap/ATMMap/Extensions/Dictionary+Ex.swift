//
//  Dictionary+Ex.swift
//  ATMMap
//
//  Created by Yury Khadatovich on 28.02.22.
//

import Foundation

extension Dictionary where Value: RangeReplaceableCollection {
    mutating func append(element: Value.Iterator.Element, toValueOfKey key: Key) {
        var value: Value = self[key] ?? Value()
        value.append(element)
        self[key] = value
    }
}

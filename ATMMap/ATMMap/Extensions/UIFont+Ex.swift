//
//  UIFont+Ex.swift
//  ATMMap
//
//  Created by Yury Khadatovich on 21.03.22.
//

import UIKit

extension UIFont {
    static let defaultFontSize: CGFloat = 14
    static let headerFontSize: CGFloat = 16
    static let cellSubviewsFont = UIFont.systemFont(ofSize: defaultFontSize, weight: .light)
    static let headerFont = UIFont.systemFont(ofSize: headerFontSize, weight: .light)
}

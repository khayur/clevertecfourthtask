//
//  UIButton+Ex.swift
//  ATMMap
//
//  Created by Yury Khadatovich on 13.03.22.
//

import Foundation
import UIKit

extension UIButton {
    func pulsate() {
        let pulse = CASpringAnimation(keyPath: "transform.scale")
        pulse.duration = UIView.AnimationOptions.animationDuration
        pulse.fromValue = 0.9
        pulse.toValue = 1.0
        layer.add(pulse, forKey: nil)
    }
}

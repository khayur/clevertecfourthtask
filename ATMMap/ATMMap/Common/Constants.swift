//
//  Constants.swift
//  ATMMap
//
//  Created by Yury Khadatovich on 23.02.22.
//

import UIKit

class Constants {

    // MARK: - Days

    enum Days {
        case monday
        case tuesday
        case wednesday
        case thursday
        case friday
        case saturday
        case sunday
        case weekend

        var shortValue: String {
            switch self {
            case .monday:
                return NSLocalizedString("MONDAY_SHORT", comment: "mondayShort")
            case .tuesday:
                return NSLocalizedString("TUESDAY_SHORT", comment: "tuesdayShort")
            case .wednesday:
                return NSLocalizedString("WEDNESDAY_SHORT", comment: "wednesdayShort")
            case .thursday:
                return NSLocalizedString("THURSDAY_SHORT", comment: "thursdayShort")
            case .friday:
                return NSLocalizedString("FRIDAY_SHORT", comment: "fridayShort")
            case .saturday:
                return NSLocalizedString("SATURDAY_SHORT", comment: "saturdayShort")
            case .sunday:
                return NSLocalizedString("SUNDAY_SHORT", comment: "sundayShort")
            case .weekend:
                return "-"
            }
        }
    }
}

//
//  LoadingView.swift
//  ATMMap
//
//  Created by Yury Khadatovich on 13.03.22.
//

import SnapKit
import UIKit

class LoadingView: UIView {

    // MARK: - Private Properties

    private lazy var loadingActivityIndicator: UIActivityIndicatorView = {
        let indicator = UIActivityIndicatorView()

        indicator.style = .large
        indicator.color = .white
        indicator.startAnimating()
        indicator.translatesAutoresizingMaskIntoConstraints = false
        addSubview(indicator)

        return indicator
    }()

    private lazy var blurEffectView: UIVisualEffectView = {
        let blurEffect = UIBlurEffect(style: .dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)

        blurEffectView.alpha = 0.8
        blurEffectView.translatesAutoresizingMaskIntoConstraints = false
        addSubview(blurEffectView)

        return blurEffectView
    }()

    // MARK: - Initializers

    required init?(coder: NSCoder) {
        super.init(coder: coder)


    }

    init() {
        super.init(frame: .zero)
        configure()
    }

    // MARK: - Private Methods

    private func configure() {
        backgroundColor = UIColor.black.withAlphaComponent(0.5)
        blurEffectView.frame = bounds
        insertSubview(blurEffectView, at: 0)
        activateConstraints()
    }

    private func activateConstraints() {
        blurEffectView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        loadingActivityIndicator.snp.makeConstraints { make in
            make.center.equalToSuperview()
        }
    }
}

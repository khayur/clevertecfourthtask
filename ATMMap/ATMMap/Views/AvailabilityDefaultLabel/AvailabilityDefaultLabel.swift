//
//  AvailabilityDefaultLabel.swift
//  ATMMap
//
//  Created by Yury Khadatovich on 21.03.22.
//

import UIKit

class AvailabilityDefaultLabel: UILabel {

    // MARK: Initializers

    init(for day: String, textColor: UIColor) {
        super.init(frame: .zero)
        text = day
        self.textColor = textColor
        configure()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Private Methods

    private func configure() {
        font = UIFont.cellSubviewsFont
        adjustsFontSizeToFitWidth = true
    }
}

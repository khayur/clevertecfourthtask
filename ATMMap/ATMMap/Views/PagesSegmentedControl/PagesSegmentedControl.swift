//
//  PagesSegmentedControl.swift
//  ATMMap
//
//  Created by Yury Khadatovich on 23.02.22.
//

import UIKit

final class PagesSegmentedControl: UISegmentedControl {

    // MARK: - Public Properties

    override var numberOfSegments: Int { 2 }

    // MARK: - Public Methods

    override func titleForSegment(at segment: Int) -> String? {
        switch segment {
        case 0:
            return Constants.Strings.titleForMapSegment
        case 1:
            return Constants.Strings.titleForListSegment
        default:
            return Constants.Strings.titleForDefaultSegment
        }
    }

    func configure() {
        for index in 0...numberOfSegments - 1 {
            insertSegment(withTitle: titleForSegment(at: index), at: index, animated: true)
        }
        backgroundColor = .white
        selectedSegmentTintColor = UIColor.appMainColor
        selectedSegmentIndex = 0
    }
}

private extension PagesSegmentedControl {
    enum Constants {
        enum Strings {
            static let titleForMapSegment = NSLocalizedString("MAP_SEGMENT_TITLE", comment: "titleForMapSegment")
            static let titleForListSegment = NSLocalizedString("LIST_SEGMENT_TITLE", comment: "titleForListSegment")
            static let titleForDefaultSegment = NSLocalizedString("DEFAULT_SEGMENT_TITLE", comment: "titleForDefaultSegment")
        }
    }
}

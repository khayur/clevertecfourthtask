//
//  ATMListCollectionViewCell.swift
//  ATMMap
//
//  Created by Yury Khadatovich on 27.02.22.
//

import SnapKit
import UIKit

final class ATMListCollectionViewCell: UICollectionViewCell, IReusableView {

    // MARK: - Public Properties

    var model: ATM?

    // MARK: - Private Properties

    private lazy var addressLabel: UILabel = {
        let label = UILabel()
        label.text = Constants.Strings.addressLabelPlaceholder
        label.numberOfLines = 4
        label.font = UIFont.cellSubviewsFont
        label.adjustsFontSizeToFitWidth = true
        label.textAlignment = .center
        label.lineBreakMode = .byTruncatingTail
        label.setContentHuggingPriority(.defaultLow, for: .vertical)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    private lazy var availabilityButton: UIButton = {
        var button = UIButton(type: .system)
        button.setTitle(Constants.Strings.availabilityButtonTitle, for: .normal)
        button.titleLabel?.adjustsFontSizeToFitWidth = true
        button.tintColor = UIColor.appMainColor
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setContentHuggingPriority(.defaultHigh, for: .vertical)
        button.setContentCompressionResistancePriority(.required, for: .vertical)
        button.addTarget(self, action: #selector(didPressAvailabilityButton), for: .touchUpInside)
        return button
    }()

    private lazy var currencyLabel: UILabel = {
        let label = UILabel()
        label.text = Constants.Strings.currencyLabelPlaceholder
        label.font = UIFont.cellSubviewsFont
        label.translatesAutoresizingMaskIntoConstraints = false
        label.setContentHuggingPriority(.defaultHigh, for: .vertical)
        label.setContentCompressionResistancePriority(.required, for: .vertical)
        return label
    }()

    private lazy var contentStackView: UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.spacing = 3
        stack.alignment = .fill
        stack.distribution = .fill
        stack.contentScaleFactor = .greatestFiniteMagnitude
        stack.translatesAutoresizingMaskIntoConstraints = false
        addSubview(stack)
        [addressLabel, availabilityButton, currencyLabel].forEach { stack.addArrangedSubview($0) }
        return stack
    }()

    private let padding = UIView.defaultPadding
    private let cornerRadius: CGFloat = UIView.cornerRadius

    // MARK: - Lifecycle

    override func prepareForReuse() {
        super.prepareForReuse()

        model = nil
    }

    // MARK: - Public Methods

    func configure() {
        backgroundColor = .white
        layer.cornerRadius = cornerRadius
        activateConstraints()

        guard let atm = model else { return }
        addressLabel.text = atm.address.addressLine
        currencyLabel.text = atm.currency.rawValue
    }

    // MARK: - Private Methods

    private func activateConstraints() {
        let insets = UIEdgeInsets(top: padding, left: padding, bottom: padding, right: padding)
        contentStackView.snp.makeConstraints { make in
            make.edges.equalToSuperview().inset(insets)
        }
    }

    // MARK: - Actions

    @objc
    private func didPressAvailabilityButton() {
        guard let model = model else { return }
        availabilityButton.pulsate()
        let availabilityView = AvailabilityView(for: model.availability.standardAvailability.day)
        availabilityView.frame = bounds
        availabilityView.center = center
        superview?.addSubview(availabilityView)
        availabilityView.alpha = 0

        UIView.animate(withDuration: UIView.AnimationOptions.animationDuration,
                       delay: 0,
                       options: .transitionCrossDissolve,
                       animations: {
            availabilityView.alpha = 1
        })
    }
}

private extension ATMListCollectionViewCell {
    enum Constants {
        enum Strings {
            static let reuseId = "ATMCell"
            static let availabilityButtonTitle = NSLocalizedString("AVAILABILITY_BUTTON_TITLE", comment: "availabilityButtonTitle")
            static let addressLabelPlaceholder = "Address"
            static let currencyLabelPlaceholder = "Currency"
        }
    }
}

//
//  IReusableView.swift
//  ATMMap
//
//  Created by Yury Khadatovich on 21.03.22.
//

import UIKit

protocol IReusableView: AnyObject {}

extension IReusableView where Self: UIView {
    static var reuseIdentifier: String {
        return String(describing: self)
    }
}

//
//  MapAnnotation.swift
//  ATMMap
//
//  Created by Yury Khadatovich on 28.02.22.
//

import MapKit
import UIKit

final class MapAnnotation: NSObject, MKAnnotation {
    var coordinate = CLLocationCoordinate2D()
    var title: String?
    var subtitle: String?
}

//
//  AvailabilityView.swift
//  ATMMap
//
//  Created by Yury Khadatovich on 27.02.22.
//

import SnapKit
import UIKit

final class AvailabilityView: UIView {

    // MARK: - Public Properties

    var model: [Day]?

    // MARK: - Private properties

    private lazy var mondayPermanentLabel: UILabel = { AvailabilityDefaultLabel(for: Constants.Days.monday.shortValue, textColor: .white) }()
    private lazy var tuesdayPermanentLabel: UILabel = { AvailabilityDefaultLabel(for: Constants.Days.tuesday.shortValue, textColor: .white) }()
    private lazy var wednesdayPermanentLabel: UILabel = { AvailabilityDefaultLabel(for: Constants.Days.wednesday.shortValue, textColor: .white) }()
    private lazy var thursdayPermanentLabel: UILabel = { AvailabilityDefaultLabel(for: Constants.Days.thursday.shortValue, textColor: .white) }()
    private lazy var fridayPermanentLabel: UILabel = { AvailabilityDefaultLabel(for: Constants.Days.friday.shortValue, textColor: .white) }()
    private lazy var saturdayPermanentLabel: UILabel = { AvailabilityDefaultLabel(for: Constants.Days.saturday.shortValue, textColor: .red) }()
    private lazy var sundayPermanentLabel: UILabel = { AvailabilityDefaultLabel(for: Constants.Days.sunday.shortValue, textColor: .red) }()

    private lazy var mondayDynamicLabel: UILabel = { AvailabilityDefaultLabel(for: Constants.Days.weekend.shortValue, textColor: .white) }()
    private lazy var tuesdayDynamicLabel: UILabel = { AvailabilityDefaultLabel(for: Constants.Days.weekend.shortValue, textColor: .white) }()
    private lazy var wednesdayDynamicLabel: UILabel = { AvailabilityDefaultLabel(for: Constants.Days.weekend.shortValue, textColor: .white) }()
    private lazy var thursdayDynamicLabel: UILabel = { AvailabilityDefaultLabel(for: Constants.Days.weekend.shortValue, textColor: .white) }()
    private lazy var fridayDynamicLabel: UILabel = { AvailabilityDefaultLabel(for: Constants.Days.weekend.shortValue, textColor: .white) }()
    private lazy var saturdayDynamicLabel: UILabel = { AvailabilityDefaultLabel(for: Constants.Days.weekend.shortValue, textColor: .white) }()
    private lazy var sundayDynamicLabel: UILabel = { AvailabilityDefaultLabel(for: Constants.Days.weekend.shortValue, textColor: .white) }()

    private lazy var permanentStackView: UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.spacing = 2
        stack.alignment = .fill
        stack.distribution = .fillEqually
        stack.setContentHuggingPriority(.defaultHigh, for: .vertical)
        stack.setContentHuggingPriority(.defaultHigh, for: .horizontal)
        [mondayPermanentLabel,
         tuesdayPermanentLabel,
         wednesdayPermanentLabel,
         thursdayPermanentLabel,
         fridayPermanentLabel,
         saturdayPermanentLabel,
         sundayPermanentLabel].forEach { stack.addArrangedSubview($0) }
        return stack
    }()

    private lazy var dynamicStackView: UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.spacing = 2
        stack.alignment = .leading
        stack.distribution = .fillEqually
        stack.setContentHuggingPriority(.defaultLow, for: .vertical)
        stack.setContentHuggingPriority(.defaultLow, for: .horizontal)
        [mondayDynamicLabel,
         tuesdayDynamicLabel,
         wednesdayDynamicLabel,
         thursdayDynamicLabel,
         fridayDynamicLabel,
         saturdayDynamicLabel,
         sundayDynamicLabel].forEach { stack.addArrangedSubview($0) }
        return stack
    }()

    private lazy var mainStackview: UIStackView = {
        let stack = UIStackView()
        stack.axis = .horizontal
        stack.spacing = 2
        stack.alignment = .center
        stack.distribution = .equalCentering
        [permanentStackView, dynamicStackView].forEach { stack.addArrangedSubview($0) }
        addSubview(stack)
        return stack
    }()

    private let padding = UIView.defaultPadding

    // MARK: - Initializers

    init(for days: [Day]?) {
        self.model = days

        super.init(frame: .zero)
        configure()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    // MARK: - Private Methods

    private func configure() {
        backgroundColor = UIColor.appMainColor.withAlphaComponent(0.9)
        let gesture = UITapGestureRecognizer(target: self, action: #selector(closeView(_:)))
        addGestureRecognizer(gesture)
        configureLabels()
        activateConstraints()
    }

    private func configureLabels() {
        guard let model = model else { return }

        for (dayIndex, day) in model.enumerated() {
            for (labelIndex, label) in dynamicStackView.arrangedSubviews.enumerated() {
                if dayIndex == labelIndex, let dayLabel = (label as? UILabel) {
                    dayLabel.text = day.openingTime.rawValue + "-" + day.closingTime.rawValue
                }
            }
        }
    }

    private func activateConstraints() {
        mainStackview.snp.makeConstraints { make in
            make.top.equalTo(self.snp.top).offset(padding)
            make.bottom.equalTo(self.snp.bottom).offset(-padding)
            make.leading.equalTo(self.snp.leading).offset(padding)
            make.trailing.equalTo(self.snp.trailing).offset(-padding)
        }
    }

    // MARK: - Actions

    @objc
    private func closeView(_ sender: UITapGestureRecognizer) {
        UIView.animate(withDuration: UIView.AnimationOptions.animationDuration,
                       delay: 0,
                       options: .transitionCrossDissolve,
                       animations: {
            self.alpha = 0
        }, completion: { _ in
            self.removeFromSuperview()
        })

    }
}

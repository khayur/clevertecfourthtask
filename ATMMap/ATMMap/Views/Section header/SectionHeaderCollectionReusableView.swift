//
//  SectionHeaderCollectionReusableView.swift
//  ATMMap
//
//  Created by Yury Khadatovich on 28.02.22.
//

import SnapKit
import UIKit

final class SectionHeaderCollectionReusableView: UICollectionReusableView {

    // MARK: - Public Properties

    static let reuseId = Constants.Strings.sectionReuseId
    
    lazy var headerLabel: UILabel = {
        let label = UILabel()
        label.text = Constants.Strings.headerLabelPlaceholder
        label.textColor = .white
        label.font = UIFont.headerFont
        addSubview(label)
        return label
    }()

    // MARK: - Private properties

    private let padding = UIView.defaultPadding
    private let cornerRadius: CGFloat = UIView.cornerRadius

    // MARK: - Initializers

    override init(frame: CGRect) {
        super.init(frame: frame)
        configure()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    // MARK: - Private Methods

    private func configure() {
        backgroundColor = .white.withAlphaComponent(0.1)
        layer.cornerRadius = cornerRadius
        activateConstraints()
    }

    private func activateConstraints() {
        headerLabel.snp.makeConstraints { make in
            make.top.equalTo(self.snp.top).offset(padding)
            make.bottom.equalTo(self.snp.bottom).offset(-padding)
            make.leading.equalTo(self.snp.leading).offset(padding)
        }
    }
}

// MARK: - Constants

private extension Constants {
    enum Strings {
        static let headerLabelPlaceholder = "Header"
        static let sectionReuseId = "SectionHeader"
    }
}

//
//  DataBaseManager.swift
//  ATMMap
//
//  Created by Yury Khadatovich on 12.03.22.
//

import CoreData
import UIKit

class DataBaseManager {

    // MARK: - Public Properties

    static let shared = DataBaseManager()
    var savedAtms = [ATMMO?]()

    // MARK: - Private Properties

    // MARK: - Initializers

    private init() { }

    // MARK: - Public Methods
    func saveATMToDB(_ atm: ATM) {
        let managedContext = getContext()
        guard let atmMO = createATMEntity(context: managedContext, atm: atm) else { return }

        do {
            try managedContext.save()
        } catch {
            managedContext.rollback()
            print("Failed saving to DB")
        }
        savedAtms.append(atmMO)
    }

    func fetchDataFromDB() {
        let managedContext = getContext()
        let request = getFetchRequest(for: Constants.atmEntityName)
        request.returnsObjectsAsFaults = false
        do {
            if let result = try managedContext.fetch(request) as? [ATMMO] {
                let decodedAvailability = try NSKeyedUnarchiver.unarchivedObject(ofClass: DayMO.self, from: result[0].value(forKey: "availability") as! Data)
            savedAtms = result
            }
            print(savedAtms.count)
        } catch {
            print("Failed fetching from DB")
        }
    }

    func clearDB() {
        let context = getContext()
        let deleteFetch = getFetchRequest(for: Constants.atmEntityName)
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: deleteFetch)
        do {
            try context.execute(deleteRequest)
        } catch let error as NSError {
            // TODO: handle the error
        }
    }

    // MARK: - Private Methods

    private func getContext() -> NSManagedObjectContext {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType) }
        let container = appDelegate.persistentContainer
        let managedContext = container.viewContext
        managedContext.mergePolicy = NSMergeByPropertyObjectTrumpMergePolicy
        return managedContext
    }

    private func getFetchRequest(for entity: String) -> NSFetchRequest<NSFetchRequestResult> {
        NSFetchRequest(entityName: entity)
    }

    private func createATMEntity(context: NSManagedObjectContext, atm: ATM) -> ATMMO? {

        let isCashInavailable = atm.services.contains(where: { $0.serviceType == .cashIn })

        
        if let atmMO = ATMMO(addressLine: atm.address.addressLine,
                             atmId: atm.atmID,
                             availability: [],
                             cashIn: isCashInavailable,
                             currency: atm.currency.rawValue,
                             latitude: atm.address.geolocation.geographicCoordinates.latitude,
                             longtitude: atm.address.geolocation.geographicCoordinates.longitude,
                             townName: atm.address.townName) {
            atmMO.availability = getAvailability(context: context, using: atm, for: atmMO)
            return atmMO
        }
        return nil
    }

    private func getAvailability(context: NSManagedObjectContext, using atm: ATM, for atmMO: ATMMO) -> [DayMO] {
        var availability = [DayMO]()

        atm.availability.standardAvailability.day.forEach({ day in
            if let dayMO = DayMO(closingTime: day.closingTime.rawValue,
                                 dayCode: day.dayCode,
                                 openingTime: day.openingTime.rawValue,
                                 atm: atmMO) {
                availability.append(dayMO)
            }
        })
        return availability
    }
}

// MARK: Constants

private extension DataBaseManager {
    enum Constants {
        static let atmEntityName = "ATMMO"
        static let dayEntityName = "DayMO"
        static let addressLine = "addressLine"
        static let atmId = "atmId"
        static let availability = "availability"
        static let cashIn = "cashIn"
        static let currency = "currency"
        static let latitude = "latitude"
        static let longtitude = "longtitude"
        static let townName = "townName"
        static let openingTime = "openingTime"
        static let closingTime = "closingTime"
        static let dayCode = "dayCode"
    }
}


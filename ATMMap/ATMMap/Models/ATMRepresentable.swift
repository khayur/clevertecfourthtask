//
//  ATMRepresentable.swift
//  ATMMap
//
//  Created by Yury Khadatovich on 12.03.22.
//

import Foundation

struct ATMRepresented {
    let atmId: String
    let cashIn: Bool
    let availability: Day
    let currency: String
    let latitude: String
    let longtitude: String
    let addressLine: String
    let townName: String
}

//
//  HomeViewController.swift
//  ATMMap
//
//  Created by Yury Khadatovich on 26.02.22.
//

import Network
import SnapKit
import UIKit

class HomeViewController: UIViewController {

    // MARK: - Private Properties

    private lazy var pagesSegmentedControl: PagesSegmentedControl = {
        let segmentedControl = PagesSegmentedControl()
        segmentedControl.configure()
        segmentedControl.addTarget(self, action: #selector(selectionChanged(_: )), for: .valueChanged)
        view.addSubview(segmentedControl)
        return segmentedControl
    }()

    private lazy var containerView: UIView = {
        let container = UIView()
        view.addSubview(container)
        return container
    }()

    private lazy var mapViewController: ATMMapViewController = {
        var viewController = ATMMapViewController()
        add(asChildViewController: viewController)
        return viewController
    }()

    private lazy var listViewController: ATMListViewController = {
        var viewController = ATMListViewController()
        add(asChildViewController: viewController)
        return viewController
    }()

    private lazy var updateButton: UIBarButtonItem = { UIBarButtonItem(barButtonSystemItem: .refresh,
                                                                       target: self,
                                                                       action: #selector(updateModel)) }()

    private lazy var loadingView: LoadingView = {
       let loading = LoadingView()
        loading.layer.zPosition = 999
        return loading
    }()

    private let networkManager = NetworkManager.shared
    private let dataBaseManager = DataBaseManager.shared
    private var model: ATMModel?

    // MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()

        fetchData()
        configureViewController()
    }

    // MARK: - Public Methods

    func forceMapCallout(for title: String) {
        pagesSegmentedControl.selectedSegmentIndex = 0
        updateView()
        mapViewController.selectAnnotation(with: title)
    }

    // MARK: - Private Methods

    private func fetchData() {
        checkConnection { $0.cancel() }

        showLoading()
        model = nil

        networkManager.fetchATMData { [weak self] atmModel, error in
            if let error = error {
                DispatchQueue.main.async {
                    self?.showErrorAlert(with: error.localizedDescription)
                }
                return
            }

            self?.model = atmModel
            self?.dataBaseManager.clearDB()
            atmModel?.data.atm.forEach { self?.dataBaseManager.saveATMToDB($0) }
            self?.listViewController.model = atmModel?.data.atm
            self?.mapViewController.model = atmModel?.data.atm

            DispatchQueue.main.async {
                self?.mapViewController.configure { self?.hideLoading() }
                self?.listViewController.prepareModelForCollectionView()
                self?.listViewController.reloadList()
                self?.updateButton.isEnabled = true
            }
        }
    }

    private func checkConnection(completion: ((NWPathMonitor) -> Void)?) {
        let monitor = NWPathMonitor()
        monitor.pathUpdateHandler = { [weak self] path in
           if path.status != .satisfied {
               DispatchQueue.main.async {
                   self?.showInternetAlert()
               }
           }
            completion?(monitor)
        }
        monitor.start(queue: DispatchQueue.global(qos: .background))
    }

    private func configureViewController() {
        view.backgroundColor = .white
        hideKeyboardWhenTappedAround()
        navigationItem.setRightBarButton(updateButton, animated: true)
        activateConstraints()
    }

    private func activateConstraints() {
        pagesSegmentedControl.snp.makeConstraints { make in
            make.top.equalTo(view.safeAreaLayoutGuide.snp.top)
            make.leading.equalToSuperview()
            make.trailing.equalToSuperview()
        }
        containerView.snp.makeConstraints { make in
            make.top.equalTo(pagesSegmentedControl.snp.bottom)
            make.leading.equalToSuperview()
            make.trailing.equalToSuperview()
            make.bottom.equalToSuperview()
        }
    }

    private func add(asChildViewController viewController: UIViewController) {
        addChild(viewController)
        containerView.addSubview(viewController.view)

        viewController.view.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }

    private func remove(asChildViewController viewController: UIViewController) {
        viewController.view.removeFromSuperview()
        viewController.removeFromParent()
    }

    private func updateView() {
        let selectedSegment = pagesSegmentedControl.selectedSegmentIndex
        switch selectedSegment {
        case 0:
            remove(asChildViewController: listViewController)
            add(asChildViewController: mapViewController)
            navigationController?.navigationBar.topItem?.title = pagesSegmentedControl.titleForSegment(at: selectedSegment)
            break
        case 1:
            remove(asChildViewController: mapViewController)
            add(asChildViewController: listViewController)
            navigationController?.navigationBar.topItem?.title = pagesSegmentedControl.titleForSegment(at: selectedSegment)
            break
        default:
            break
        }
    }

    private func showInternetAlert() {
        let alert = UIAlertController(title: Constants.Strings.internetAlertTitle,
                                      message: Constants.Strings.internetAlertMessage,
                                      preferredStyle: .alert)
        let action = UIAlertAction(title: Constants.Strings.internetAlertButtonTitle,
                                   style: .default,
                                   handler: { [weak self] _ in
            self?.updateButton.isEnabled = true
            self?.hideLoading()
        })
        alert.addAction(action)
        present(alert, animated: true, completion: nil)
    }

    private func showErrorAlert(with message: String) {
        let alert = UIAlertController(title: Constants.Strings.errorAlertTitle,
                                      message: message,
                                      preferredStyle: .alert)
        let actionClose = UIAlertAction(title: Constants.Strings.errorAlertCloseButtonTitle,
                                        style: .cancel,
                                        handler: { [weak self] _ in
            self?.updateButton.isEnabled = true
            self?.hideLoading()
        })
        let actionRepeat = UIAlertAction(title: Constants.Strings.errorAlertRepeatButtonTitle,
                                         style: .default,
                                         handler: { [weak self] _ in
                                        self?.fetchData() })

        alert.addAction(actionClose)
        alert.addAction(actionRepeat)
        present(alert, animated: true, completion: nil)
    }

    private func showLoading() {
        containerView.addSubview(loadingView)
        loadingView.frame = containerView.bounds
        loadingView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }

    private func hideLoading() {
        loadingView.removeFromSuperview()
    }

    // MARK: - Actions

    @objc
    func selectionChanged(_ sender: UISegmentedControl) {
        updateView()
    }

    @objc
    func updateModel() {
        updateButton.isEnabled = false

        fetchData()
    }
}

// MARK: - Constants

private extension HomeViewController {
    enum Constants {
        enum Strings {
            static let internetAlertMessage = NSLocalizedString("INTERNET_ALERT_MESSAGE", comment: "internetAlertMessage")
            static let internetAlertTitle = NSLocalizedString("INTERNET_ALERT_TITLE", comment: "internetAlertTitle")
            static let internetAlertButtonTitle = NSLocalizedString("INTERNET_ALERT_BUTTON_TITLE", comment: "internetAlertButtonTitle")
            static let errorAlertTitle = NSLocalizedString("ERROR_ALERT_TITLE", comment: "errorAlertTitle")
            static let errorAlertRepeatButtonTitle = NSLocalizedString("ERROR_ALERT_REPEAT_BUTTON_TITLE", comment: "internetAlertButtonTitle")
            static let errorAlertCloseButtonTitle = NSLocalizedString("ERROR_ALERT_CLOSE_BUTTON_TITLE", comment: "internetAlertButtonTitle") 
        }
    }
}

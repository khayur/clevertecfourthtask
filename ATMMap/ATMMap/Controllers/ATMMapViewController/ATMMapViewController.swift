//
//  ATMMapViewController.swift
//  ATMMap
//
//  Created by Yury Khadatovich on 24.02.22.
//

import MapKit
import SnapKit
import UIKit

class ATMMapViewController: UIViewController {

    // MARK: - Public Properties

    var model: [ATM]?

    // MARK: - Private Properties

    private lazy var mapView: MKMapView = {
        let mapView = MKMapView()
        mapView.delegate = self
        mapView.register(MKMarkerAnnotationView.self,
                         forAnnotationViewWithReuseIdentifier: NSStringFromClass(MapAnnotation.self))
        view.addSubview(mapView)
        return mapView
    }()

    private lazy var routeButton: UIButton = {
        let button = UIButton()
        var configuration = UIButton.Configuration.plain()
        configuration.contentInsets = NSDirectionalEdgeInsets(top: UIView.defaultPadding * 4,
                                                              leading: UIView.defaultPadding * 4,
                                                              bottom: UIView.defaultPadding * 4,
                                                              trailing: UIView.defaultPadding * 4)
        button.configuration = configuration
        button.layer.cornerRadius = UIView.cornerRadius
        button.setTitleColor(.white, for: .normal)
        button.setTitleColor(.white, for: .highlighted)
        button.setTitle(Constants.Strings.routeButtonText, for: .normal)
        button.titleLabel?.font = UIFont.headerFont
        button.backgroundColor = UIColor.appMainColor
        button.isEnabled = false
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(didPressRouteButton), for: .touchUpInside)
        mapView.addSubview(button)
        return button
    }()

    private lazy var userLocationButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = UIColor.appMainColor
        button.setImage(Constants.Images.userLocationButtonImage, for: .normal)
        button.tintColor = .white
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(didPressUserLocationButton), for: .touchUpInside)
        mapView.addSubview(button)
        return button
    }()

    private var locationManager = LocationManager.shared
    private var currentLocationStr = Constants.Strings.currentLocation
    private var selectedATMCoordinates: CLLocationCoordinate2D?

    // MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()

        configure(completion: nil)
    }
    // MARK: - Public Methods

    func configure(completion: (() -> Void)?) {
        setupInterface()
        setupLocationManager()
        addAnnotations(completion: completion)
        completion?()
    }

    func selectAnnotation(with title: String) {
        guard let annotation = mapView.annotations.first(where: { $0.title == title }) else { return }
        let center = CLLocationCoordinate2D(latitude: annotation.coordinate.latitude, longitude: annotation.coordinate.longitude)
        let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
        mapView.setRegion(region, animated: true)
        mapView.selectAnnotation(annotation, animated: true)
    }

    // MARK: - Private Methods

    private func activateConstraints() {
        mapView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }

        routeButton.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.bottom.equalTo(mapView.snp.bottom).offset(-UIView.defaultPadding * 4)
        }

        userLocationButton.snp.makeConstraints { make in
            make.height.equalTo(routeButton.snp.height)
            make.width.equalTo(routeButton.snp.height)
            make.trailing.equalTo(mapView.snp.trailing).offset(-UIView.defaultPadding * 4)
            make.leading.greaterThanOrEqualTo(routeButton.snp.trailing).offset(UIView.defaultPadding)
            make.bottom.equalTo(mapView.snp.bottom).offset(-UIView.defaultPadding * 4)
        }
        view.layoutIfNeeded()
    }

    private func setupInterface() {
        activateConstraints()
        userLocationButton.layer.cornerRadius = userLocationButton.frame.height / 2
    }

    private func setupLocationManager() {
        locationManager.setDelegate(self)
        locationManager.startUpdatingLocation()
    }

    private func addAnnotations(completion: (() -> Void)?) {
        guard let model = model else { return }
        model.forEach { [weak self] atm in
            let latitude = Double(atm.address.geolocation.geographicCoordinates.latitude) ?? 0
            let longtitude = Double(atm.address.geolocation.geographicCoordinates.longitude) ?? 0
            let annotation = MapAnnotation()
            annotation.title = atm.address.addressLine
        
            annotation.coordinate = CLLocationCoordinate2D(latitude: latitude, longitude: longtitude)
            self?.mapView.addAnnotation(annotation)
        }
        completion?()
    }

    private func showLocationAlert() {
        let alert = UIAlertController(title: Constants.Strings.locationAlertTitle,
                                      message: Constants.Strings.locationAlertMessage,
                                      preferredStyle: .alert)
        let okAction = UIAlertAction(title: Constants.Strings.locationAlertAction, style: .default, handler: nil)
        alert.addAction(okAction)

        present(alert, animated: true, completion: nil)
    }

    @objc
    private func didPressRouteButton() {
        guard let selectedATMCoordinates = selectedATMCoordinates else { return }
        routeButton.pulsate()
        let mapItem = MKMapItem(placemark: MKPlacemark(coordinate: selectedATMCoordinates))
        mapItem.openInMaps(launchOptions: [MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeDriving])
    }

    @objc
    private func didPressUserLocationButton() {
        userLocationButton.pulsate()

        if let userLocation = locationManager.getLocation()?.coordinate,
           CLLocationManager.locationServicesEnabled() {
            let viewRegion = MKCoordinateRegion(center: userLocation, latitudinalMeters: 200, longitudinalMeters: 200)
            mapView.setRegion(viewRegion, animated: false)
        } else if !CLLocationManager.locationServicesEnabled() {
            showLocationAlert()
        }
    }
}

// MARK: - CLLocationManagerDelegate

extension ATMMapViewController: CLLocationManagerDelegate {

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let userLocation = locations[0] as CLLocation
        let center = CLLocationCoordinate2D(latitude: userLocation.coordinate.latitude, longitude: userLocation.coordinate.longitude)
        let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
        mapView.setRegion(region, animated: true)
        mapView.showsUserLocation = true
        manager.stopUpdatingLocation()
    }

    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        assertionFailure("Error - locationManager: \(error.localizedDescription)")
    }
}

// MARK: - MKMapViewDelegate

extension ATMMapViewController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {

        guard !annotation.isKind(of: MKUserLocation.self) else { return nil }

        var annotationView: MKAnnotationView?

        if let annotation = annotation as? MapAnnotation {
            annotationView = setupAnnotationView(for: annotation, on: mapView)
        }

        return annotationView
    }

    private func setupAnnotationView(for annotation: MapAnnotation, on mapView: MKMapView) -> MKAnnotationView {
        let identifier = NSStringFromClass(MapAnnotation.self)
        let view = mapView.dequeueReusableAnnotationView(withIdentifier: identifier, for: annotation)
        if let markerAnnotationView = view as? MKMarkerAnnotationView {
            markerAnnotationView.animatesWhenAdded = true
            markerAnnotationView.canShowCallout = true
            markerAnnotationView.markerTintColor = UIColor.appMainColor

            /*
             Add a detail disclosure button to the callout, which will open a new view controller or a popover.
             When the detail disclosure button is tapped, use mapView(_:annotationView:calloutAccessoryControlTapped:)
             to determine which annotation was tapped.
             If you need to handle additional UIControl events, such as `.touchUpOutside`, you can call
             `addTarget(_:action:for:)` on the button to add those events.
             */
//            let rightButton = UIButton(type: .detailDisclosure)
//            markerAnnotationView.rightCalloutAccessoryView = rightButton

            let selectedATM = model?.first(where: { $0.address.addressLine == view.annotation?.title })

            markerAnnotationView.detailCalloutAccessoryView = setupMarkerDetails(for: selectedATM)
        }

        return view
    }

    private func setupMarkerDetails(for atm: ATM?) -> UIStackView {
        guard let atm = atm else { return UIStackView() }

        let ATMAvailability = atm.availability.standardAvailability.day
        let isCashInavailable = atm.services.contains(where: { $0.serviceType == .cashIn })
        let currency = atm.currency

        let availabilityView = AvailabilityView(for: ATMAvailability)
        availabilityView.isUserInteractionEnabled = false

        let cashInLabel = UILabel()
        if isCashInavailable {
            cashInLabel.text = Constants.Strings.cashInYes
        } else {
            cashInLabel.text = Constants.Strings.cashInNo
        }
        cashInLabel.font = UIFont.cellSubviewsFont

        let currencyLabel = UILabel()
        currencyLabel.text = currency.rawValue
        currencyLabel.font = UIFont.cellSubviewsFont

        let detailsStack = UIStackView()
        [availabilityView, cashInLabel, currencyLabel].forEach({ detailsStack.addArrangedSubview($0) })
        detailsStack.axis = .vertical
        detailsStack.alignment = .leading
        detailsStack.distribution = .fill
        detailsStack.spacing = 5

        return detailsStack
    }

    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        guard let markerView = view as? MKMarkerAnnotationView,
              let selectedATM = model?.first(where: { $0.address.addressLine == markerView.annotation?.title }) else { return }

        let latitudeString = selectedATM.address.geolocation.geographicCoordinates.latitude
        let longtitudeString = selectedATM.address.geolocation.geographicCoordinates.longitude

        if let latitude = CLLocationDegrees(latitudeString),
           let longtitude = CLLocationDegrees(longtitudeString) {
            selectedATMCoordinates = CLLocationCoordinate2D(latitude: latitude, longitude: longtitude)
            routeButton.isEnabled = true
        }
    }

    func mapView(_ mapView: MKMapView, didDeselect view: MKAnnotationView) {
        selectedATMCoordinates = nil
        routeButton.isEnabled = false
    }
}

// MARK: - Constants

private extension ATMMapViewController {
    enum Constants {
        enum Strings {
            static let routeButtonText =  NSLocalizedString("ROUTE_BUTTON_TITLE", comment: "routeButtonTitle") 
            static let currentLocation =  NSLocalizedString("CURRENT_LOCATION", comment: "currentLocation")
            static let cashInYes = NSLocalizedString("CASH_IN_YES", comment: "cashInYes")
            static let cashInNo = NSLocalizedString("CASH_IN_NO", comment: "cashInNo")
            static let locationAlertTitle = NSLocalizedString("LOCATION_ALERT_TITLE", comment: "Location Services disabled title")
            static let locationAlertMessage = NSLocalizedString("LOCATION_ALERT_MESSAGE", comment: "Location Services disabled message")
            static let locationAlertAction = NSLocalizedString("LOCATION_ALERT_ACTION", comment: "Location Services disabled action")
        }

        enum Images {
            static let userLocationButtonImage = UIImage(systemName: "location")!
        }
    }
}

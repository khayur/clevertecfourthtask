//
//  ATMListViewController.swift
//  ATMMap
//
//  Created by Yury Khadatovich on 23.02.22.
//

import SnapKit
import UIKit

class ATMListViewController: UIViewController {

    // MARK: - Public properties

    var model: [ATM]?

    // MARK: - Private properties

    private lazy var ATMCollectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()

        layout.sectionInset = UIEdgeInsets(top: Constants.Dimensions.cellsSpacing / 2,
                                           left: Constants.Dimensions.cellsSpacing,
                                           bottom: Constants.Dimensions.cellsSpacing / 2,
                                           right: Constants.Dimensions.cellsSpacing)
        let cellWidth = (view.bounds.width
                         - layout.sectionInset.left
                         - layout.sectionInset.right
                         - Constants.Dimensions.cellsSpacing
                         * (CGFloat(numberOfItemsInSection) + 1)) / CGFloat(numberOfItemsInSection)
        let cellHeight = cellWidth
        layout.minimumLineSpacing = Constants.Dimensions.cellsSpacing
        layout.minimumInteritemSpacing = Constants.Dimensions.cellsSpacing
        layout.itemSize = CGSize(width: cellWidth, height: cellHeight)
        layout.headerReferenceSize = CGSize(width: self.view.frame.size.width, height: Constants.Dimensions.headerHeight)

        let collectionView = UICollectionView(frame: self.view.frame, collectionViewLayout: layout)
        collectionView.register(ATMListCollectionViewCell.self, forCellWithReuseIdentifier: ATMListCollectionViewCell.reuseIdentifier)
        collectionView.register(SectionHeaderCollectionReusableView.self,
                                forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader,
                                withReuseIdentifier: SectionHeaderCollectionReusableView.reuseId)
        collectionView.backgroundColor = UIColor.appMainColor
        collectionView.keyboardDismissMode = .onDrag
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.translatesAutoresizingMaskIntoConstraints = false

        view.addSubview(collectionView)
        return collectionView
    }()

    private lazy var searchBar: UISearchBar = {
        let bar = UISearchBar()
        bar.showsCancelButton = true
        bar.delegate = self
        bar.sizeToFit()
        bar.isTranslucent = true
        bar.placeholder = Constants.Strings.searchBarPlaceholder
        view.addSubview(bar)

        return bar
    }()

    private let numberOfItemsInSection = 3
    private var townNames = [String]()
    private var modelForWork = [String: [ATM]]()

    // MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()

        configureViewController()
    }

    // MARK: - Public Methods

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)

        view.endEditing(true)
    }

    func prepareModelForCollectionView() {
            modelForWork.removeAll()
            townNames.removeAll()

            model?.forEach({ atm in
                if !townNames.contains(atm.address.townName), !atm.address.townName.isEmpty {
                    townNames.append(atm.address.townName)
                } else if !townNames.contains(atm.address.townName), atm.address.townName.isEmpty {
                    townNames.append(atm.address.streetName)
                }
                modelForWork.append(element: atm, toValueOfKey: atm.address.townName)
                modelForWork.append(element: atm, toValueOfKey: atm.address.streetName)
            })
            townNames = townNames.sorted { $0 < $1 }

            modelForWork.forEach({ key, value in
                modelForWork.updateValue(value.sorted { $0.atmID < $1.atmID }, forKey: key)
            })
        }

    func reloadList() {
        searchBar.endEditing(true)
        ATMCollectionView.reloadData()
    }

    // MARK: - Private methods

    private func configureViewController() {
        activateConstraints()
        prepareModelForCollectionView()
    }

    private func activateConstraints() {
        ATMCollectionView.snp.makeConstraints { make in
            make.top.equalTo(searchBar.snp.bottom)
            make.leading.equalTo(view.snp.leading)
            make.trailing.equalTo(view.snp.trailing)
            make.bottom.equalTo(view.snp.bottom)
        }
        searchBar.snp.makeConstraints { make in
            make.top.equalTo(view.snp.top)
            make.leading.equalTo(view.snp.leading)
            make.trailing.equalTo(view.snp.trailing)
        }
    }
}

// MARK: - CollectionView DataSource

extension ATMListViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let key = townNames[section]
        return modelForWork[key]?.count ?? 0
    }

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return townNames.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ATMListCollectionViewCell.reuseIdentifier, for: indexPath)

        if let atmCell = cell as? ATMListCollectionViewCell {
            let key = townNames[indexPath.section]
            atmCell.model = modelForWork[key]?[indexPath.row]
            atmCell.configure()
        }

        return cell
    }

    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind,
                                                                     withReuseIdentifier: SectionHeaderCollectionReusableView.reuseId, for: indexPath)
        if  let sectionHeader = header as? SectionHeaderCollectionReusableView {
            sectionHeader.headerLabel.text = townNames[indexPath.section]
            return sectionHeader
        }
        return UICollectionReusableView()
    }
}

// MARK: - CollectionView Delegate

extension ATMListViewController: UICollectionViewDelegate {

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let homeController = parent as? HomeViewController else { return }

        let key = townNames[indexPath.section]

        homeController.forceMapCallout(for: modelForWork[key]?[indexPath.row].address.addressLine ?? "")
    }
}

// MARK: - SearchBar delegate

extension ATMListViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        guard let searchedTownIndex = townNames.firstIndex(where: { $0.lowercased().starts(with: searchText.lowercased()) }) else { return }

        ATMCollectionView.scrollToItem(at: IndexPath(item: 0, section: searchedTownIndex),
                                       at: .top,
                                       animated: true)
    }

    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.endEditing(true)
    }
}

// MARK: - Constants

private extension ATMListViewController {
    enum Constants {
        enum Strings {
            static let searchBarPlaceholder = NSLocalizedString("SEARCH_BAR_PLACEHOLDER",
                                                                comment: "searchBarPlaceholder")
        }
        enum Dimensions {
            static let cellsSpacing: CGFloat = 10
            static let headerHeight: CGFloat = 30
        }
    }
}

//
//  LocationManager.swift
//  ATMMap
//
//  Created by Yury Khadatovich on 12.03.22.
//

import Foundation
import CoreLocation
import UIKit

class LocationManager {

    // MARK: - Public Properties

    static let shared = LocationManager()

    // MARK: - Private Properties

    private let manager: CLLocationManager
    
    // MARK: - Initializers

    private init() {
        manager = CLLocationManager()
        manager.desiredAccuracy = kCLLocationAccuracyBest
        manager.activityType = .automotiveNavigation
        manager.requestAlwaysAuthorization()
    }

    // MARK: - Public Methods

    func setDelegate(_ delegate: CLLocationManagerDelegate) {
        manager.delegate = delegate
    }

    func startUpdatingLocation() {
        if CLLocationManager.locationServicesEnabled() {
            manager.startUpdatingLocation()
        }
    }

    func stopUpdatingLocation() {
        manager.stopUpdatingLocation()
    }

    func getLocation() -> CLLocation? { manager.location }
}

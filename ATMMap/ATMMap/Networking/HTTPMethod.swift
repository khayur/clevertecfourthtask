//
//  HTTPMethod.swift
//  ATMMap
//
//  Created by Yury Khadatovich on 27.02.22.
//

import UIKit

enum HTTPMethod: String {
    case get
    case post
    case put
    case patch
    case delete

    var name: String {
        return self.rawValue.uppercased()
    }
}

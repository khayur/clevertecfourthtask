//
//  NetworkError.swift
//  ATMMap
//
//  Created by Yury Khadatovich on 27.02.22.
//

import Foundation

enum NetworkError: Error {
    case invalidUrl
    case invalidData
}

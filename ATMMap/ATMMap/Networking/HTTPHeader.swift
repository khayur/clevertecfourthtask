//
//  HTTPHeader.swift
//  ATMMap
//
//  Created by Yury Khadatovich on 27.02.22.
//

import Foundation

enum HTTPHeader: String {
    case contentType = "Content-Type"
}

//
//  HTTPHeaderValue.swift
//  ATMMap
//
//  Created by Yury Khadatovich on 27.02.22.
//

import Foundation

enum HTTPHeaderValue: String {
    case json = "application/json; charset=utf-8"
}

//
//  ATMMO+CoreDataProperties.swift
//  ATMMap
//
//  Created by Yury Khadatovich on 12.03.22.
//
//

import Foundation
import CoreData


extension ATMMO {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<ATMMO> {
        return NSFetchRequest<ATMMO>(entityName: "ATMMO")
    }

    @NSManaged public var addressLine: String?
    @NSManaged public var atmId: String?
    @NSManaged public var availability: [DayMO]?
    @NSManaged public var cashIn: Bool
    @NSManaged public var currency: String?
    @NSManaged public var latitude: String?
    @NSManaged public var longtitude: String?
    @NSManaged public var townName: String?
    @NSManaged public var availabilityMO: NSSet?


}

// MARK: Generated accessors for availabilityMO
extension ATMMO {

    @objc(addAvailabilityMOObject:)
    @NSManaged public func addToAvailabilityMO(_ value: DayMO)

    @objc(removeAvailabilityMOObject:)
    @NSManaged public func removeFromAvailabilityMO(_ value: DayMO)

    @objc(addAvailabilityMO:)
    @NSManaged public func addToAvailabilityMO(_ values: NSSet)

    @objc(removeAvailabilityMO:)
    @NSManaged public func removeFromAvailabilityMO(_ values: NSSet)

}

extension ATMMO : Identifiable {

}

//
//  DayMO+CoreDataProperties.swift
//  ATMMap
//
//  Created by Yury Khadatovich on 12.03.22.
//
//

import Foundation
import CoreData

extension DayMO {

    @nonobjc
    public class func fetchRequest() -> NSFetchRequest<DayMO> {
        return NSFetchRequest<DayMO>(entityName: "DayMO")
    }

    @NSManaged public var closingTime: String?
    @NSManaged public var dayCode: String?
    @NSManaged public var openingTime: String?
    @NSManaged public var atm: ATMMO?

    
}

extension DayMO: Identifiable {

}

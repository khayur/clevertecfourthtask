# README #

ATMMap application created for possibility to see ATMs of Belarusbank. ATMs presented in two views: map and list.

## Technical Info ##
> 1. Architecture: CocoaMVC
> 2. Layout: Autolayout programmatically, using snapKit framework
> 3. Using CoreData for saving ATMs info
> 4. Localization to Ru and En
> 5. Errors Hadling with UIAlertControllers

## Features ##

### > MAP: ###
>> 1. using current user' location. If location is unavailable - map zooms to whole Belarus
>> 2. annotations view' presents info about ATM (place, availability, cashIn service, ATM' currency)
>> 3. building route to selected ATM using default map application

### > LIST: ###
>> 1. ATMs divided by sections (3 ATMs in row) by town names
>> 2. sections sorted by townNames
>> 3. inside section sorted by atmID
>> 4. possibility to see working schedule by pressing "Timetable" button
>> 5. press on ATM cell redirects on mapView to ATM' point


### > TODO ###

>> 1. Fetching ATMs from CoreData and using saved entities if internet is unavailable
>> 2. To add bank branches
>> 3. To add infokiosks
>> 4. Filter possibility
>> 5. Search by different parameters
>> 6. Detailed entity info at new screen


### Screenshots ###
![Scheme](ATMMap/ATMMap/Screenshots/Simulator Screen Shot - iPhone 13 Pro Max - 2022-03-13 at 14.04.49.png)
![Scheme](ATMMap/ATMMap/Screenshots/Simulator Screen Shot - iPhone 13 Pro Max - 2022-03-13 at 10.28.41.png)
![Scheme](ATMMap/ATMMap/Screenshots/Simulator Screen Shot - iPhone 13 Pro Max - 2022-03-13 at 10.29.30.png)
![Scheme](ATMMap/ATMMap/Screenshots/Simulator Screen Shot - iPhone 13 Pro Max - 2022-03-13 at 10.30.31.png)
![Scheme](ATMMap/ATMMap/Screenshots/Simulator Screen Shot - iPhone 13 Pro Max - 2022-03-13 at 10.30.47.png)
![Scheme](ATMMap/ATMMap/Screenshots/Simulator Screen Shot - iPhone 13 Pro Max - 2022-03-13 at 10.20.38.png)
![Scheme](ATMMap/ATMMap/Screenshots/photo_2022-03-13 13.32.02.jpeg)
![Scheme](ATMMap/ATMMap/Screenshots/photo_2022-03-13 13.32.05.jpeg)
![Scheme](ATMMap/ATMMap/Screenshots/photo_2022-03-13 13.32.08.jpeg)


### SUPPORT TO ###
1. VISA CARD: 4585 2200 0746 3614 YURY KHADATOVICH
2. IBAN: BY29ALFA301430A8KY0010270000